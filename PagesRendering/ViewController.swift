//
//  ViewController.swift
//  PagesRendering
//
//  Created by Lera Mozgovaya on 1/28/19.
//  Copyright © 2019 Lera Mozgovaya. All rights reserved.
//

import UIKit

let dataSourceURL = URL(string:"http://www.raywenderlich.com/downloads/ClassicPhotosDictionary.plist")!

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        fetchPhotos()
        collectionView.delegate = self
        collectionView.dataSource = self

        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBOutlet weak var collectionView: UICollectionView!
    
    var photos = [PhotoRecord]()
    
    func fetchPhotos() {
        
        let task = URLSession(configuration: .default).dataTask(with: dataSourceURL) { (data, response, err) in
            
            let alertController = UIAlertController(title: "Oops!",
                                                    message: "There was an error fetching photo details.",
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default)
            alertController.addAction(okAction)
            
            if let data = data {
                
                do {
                   let downloadedDic = try PropertyListSerialization.propertyList(from: data, options: [], format: nil) as! [String: String]
                    
                    for item in downloadedDic {
                        
                        if let url = URL(string: item.value) {
                            let photo = PhotoRecord(name: item.key, url: url)
                            self.photos.append(photo)
                        }
                    }
                    
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                        
                    }
                    self.downloadPhotos()

                }
                catch {
                    
                    DispatchQueue.main.async {
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            }
            
            if err != nil {
                DispatchQueue.main.async {
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
        
        task.resume()
    }
    
    func startDownloading(photo: PhotoRecord, indexPath: IndexPath, completion: (() -> Void)?) {
        
        let queue = DispatchQueue(label: "Custom Queue", qos: .userInitiated, attributes: .concurrent)
        
        let workItem = DispatchWorkItem {
            
            guard let imageData = try? Data(contentsOf: photo.url) else { return }
            
            photo.image = UIImage(data: imageData, scale: CGFloat(Float.random(in: 0.5..<1)))
            photo.state = .downoaded
            
            DispatchQueue.main.async(execute: {
                self.collectionView.reloadItems(at: [indexPath])
            })
            
            completion?()
        }
        
        queue.async(execute: workItem)
    }
    
    func downloadPhotos() {
        
        let group = DispatchGroup()
        
        for (idx, photo) in photos.enumerated() {
            group.enter()
        
            startDownloading(photo: photo, indexPath: IndexPath(item: idx, section: 0), completion: {
                group.leave()
            })
        }
        
        group.notify(queue: .main) {
            
            let layout = PinterestLayout()
            layout.delegate = self
            
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
                
                self.collectionView.collectionViewLayout.invalidateLayout()
                
                self.collectionView.collectionViewLayout = layout
            
            }, completion: { (comp) in
                self.collectionView.reloadData()
            })
        }
    }
}

extension ViewController: UICollectionViewDataSource {
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell: PhotoCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: PhotoCollectionCell.self), for: indexPath) as? PhotoCollectionCell else { return UICollectionViewCell() }
    
        let photo = photos[indexPath.row]
        
        cell.photo = photo
        
        return cell
    }
}

extension ViewController: PinterestLayoutDelegate {
    
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        guard let height = photos[indexPath.item].image?.size.height else {
            return 0
        }
        
        return height
    }
}

extension ViewController: UICollectionViewDelegate {
    
}

extension ClosedRange where Bound : FloatingPoint {
    public func random() -> Bound {
        let max = UInt32.max
        return
            Bound(arc4random_uniform(max)) /
                Bound(max) *
                (upperBound - lowerBound) +
        lowerBound
    }
}
